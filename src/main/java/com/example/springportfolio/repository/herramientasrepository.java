package com.example.springportfolio.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface herramientasrepository<herramientasmodel> extends JpaRepository<herramientasmodel, Integer> {
    List<herramientasmodel> findAll();

    void deleteById(int id);

    <herramientasmodel> herramientasmodel save(S herramientas);

    Optional<Object> findById(int id);
}
