package com.example.springportfolio.repository;

import com.example.springportfolio.model.lenguajesmodel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface lenguajesrepository extends JpaRepository<lenguajesmodel, Long> {
}
