package com.example.springportfolio.controller;


import com.example.springportfolio.model.lenguajesmodel;
import com.example.springportfolio.services.contactoservices;
import com.example.springportfolio.services.herramientasservices;
import com.example.springportfolio.services.lenguajesservices;
import com.example.springportfolio.services.proyectosservices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class controlador {

    @Autowired
    private contactoservices contactoService;

    @Autowired
    private proyectosservices proyectosService;

    @Autowired
    private lenguajesservices lenguajesService;

    @Autowired
    private herramientasservices herramientasService;



    @RequestMapping("/")
    String index(Model model) {
       //List<lenguajesmodel> lenguajesmodel = lenguajesService.get();

        model.addAttribute("contacto",contactoService.getAllContactos());
        model.addAttribute("lenguajes", lenguajesService.getAllLenguajesProgramacion());
        model.addAttribute("proyectos", proyectosService.getAllProyectos());
        model.addAttribute("herramientas", herramientasService.getAllHerramientasOffice());

        return "index";
    }
    }