package com.example.springportfolio.services;

import com.example.springportfolio.model.lenguajesmodel;
import com.example.springportfolio.repository.lenguajesrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class lenguajesservices {

    @Autowired
    private lenguajesrepository lenguajesProgramacionRepository;

    public List<lenguajesmodel> getAllLenguajesProgramacion() {
        return lenguajesProgramacionRepository.findAll();
    }

    public lenguajesmodel getLenguajesProgramacionById(int id) {
        return lenguajesProgramacionRepository.findById((long) id).orElse(null);
    }

    public lenguajesmodel saveOrUpdateLenguajesProgramacion(lenguajesmodel lenguajesProgramacion) {
        return lenguajesProgramacionRepository.save(lenguajesProgramacion);
    }

    public void deleteLenguajesProgramacion(int id) {
        lenguajesProgramacionRepository.deleteById((long) id);
    }
}
