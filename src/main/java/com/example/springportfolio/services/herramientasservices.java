package com.example.springportfolio.services;


import com.example.springportfolio.repository.herramientasrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class
herramientasservices {

    @Autowired
    private herramientasrepository herramientasOfficeRepository;

    public <herramientasmodel> List<herramientasmodel> getAllHerramientasOffice() {
        return (List<herramientasmodel>) herramientasOfficeRepository.findAll();
    }

    public <herramientasmodel> herramientasmodel getHerramientasOfficeById(int id) {
        return (herramientasmodel) herramientasOfficeRepository.findById(id).orElse(null);
    }

    public <herramientasmodel> herramientasmodel saveOrUpdateHerramientasOffice(herramientasmodel herramientas) {
        return herramientasOfficeRepository.save(herramientas);
    }

    public void deleteHerramientasOffice(int id) {
        herramientasOfficeRepository.deleteById(id);
    }
}

