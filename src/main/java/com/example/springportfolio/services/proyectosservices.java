package com.example.springportfolio.services;

import com.example.springportfolio.model.proyectosmodel;
import com.example.springportfolio.repository.proyectosrepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class proyectosservices{

    @Autowired
    private proyectosrepository Proyectosrepository;

    public List<proyectosmodel> getAllProyectos() {
        return Proyectosrepository.findAll();
    }

    public proyectosmodel getProyectosById(int id) {
        return Proyectosrepository.findById(id).orElse(null);
    }

    public proyectosmodel saveOrUpdateProyectos(proyectosmodel proyectos) {
        return Proyectosrepository.save(proyectos);
    }

    public void deleteProyectos(int id) {
        Proyectosrepository.deleteById(id);
    }
}
