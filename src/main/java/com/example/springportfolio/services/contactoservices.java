package com.example.springportfolio.services;

import com.example.springportfolio.model.contactomodel;
import com.example.springportfolio.repository.contactorepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class contactoservices {

    @Autowired
    private contactorepository contactoRepository;

    public List<contactomodel> getAllContactos() {
        return contactoRepository.findAll();
    }

    public contactomodel getContactoById(int id) {
        return contactoRepository.findById(id).orElse(null);
    }

    public contactomodel saveOrUpdateContacto(contactomodel contacto) {
        return contactoRepository.save(contacto);
    }

    public void deleteContacto(int id) {
        contactoRepository.deleteById(id);
    }
}
