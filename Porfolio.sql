CREATE DATABASE Porfolio;
-- Tabla 'about'
CREATE TABLE about (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    profession VARCHAR(30) NOT NULL,
    bio TEXT NOT NULL,
    current_study VARCHAR(55) NOT NULL
);

INSERT INTO about (name, profession, bio, current_study) VALUES
('Valentina Forero', 'Técnico superior en Diseño de elementos mecanicos', 
'Técnico superior en Diseño de elementos mecanicos para su fabricación con máquinas herramientas CNC con una sólida formación. 
Mi enfoque práctico y habilidades en diseño me han permitido contribuir significativamente a proyectos.', 'Técnico superior en
 desarrollo de aplicaciones multiplataforma 2023 - 2025');

-- Tabla 'services'
CREATE TABLE services (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL,
    link VARCHAR(255)
);

INSERT INTO services (title, description, link) VALUES
('Dibujo tecnico en SolidWorks', 'Ofrezco experiencia en la creación de modelos 3D y dibujos técnicos utilizando SolidWorks. Transformo conceptos en diseños detallados listos para su implementación', '#'),
('Desarrollo web con HTML y CSS', 'Diseño y desarrollo de páginas web a medida utilizando tecnologías modernas como HTML y CSS. Enfocado en crear experiencias web atractivas y funcionales.', '#'),
('Programación en Maquinas CNC', 'Experiencia en programación CNC para maquinaria industrial. Transformo diseños digitales en productos físicos mediante la programación y operación de máquinas CNC.', '#'),
('Programación en Java', 'Desarrollo de aplicaciones y soluciones utilizando el lenguaje de programación Java. Desde aplicaciones de escritorio hasta sistemas empresariales, proporcionando soluciones efectivas y eficientes.', '#');

-- Tabla 'gallery'
CREATE TABLE gallery (
    id INT AUTO_INCREMENT PRIMARY KEY,
    image_path VARCHAR(255) NOT NULL
);

INSERT INTO gallery (image_path) VALUES
('galeria1.png'),
('galeria2.png'),
('galeria5.png'),
('galeria4.jpg'),
('galeria6.png'),
('galeria3.png');

-- Tabla 'contact'
CREATE TABLE contact (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    message TEXT NOT NULL
);
